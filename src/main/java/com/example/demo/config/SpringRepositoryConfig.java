/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.InMemoryItemRepository;
import com.example.demo.persistence.ItemRepository;

// Declare as a configuration class
@Configuration
public class SpringRepositoryConfig {
	
	// Declare the item repository bean
	@Bean 
	public ItemRepository itemRepository() {
		return new InMemoryItemRepository();
	}
	
}